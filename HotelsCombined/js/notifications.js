var HC = HC || {};

HC.Notifications = {

	updateTopDestinations: function(){

		var topDestinations = '';
		$.ajax({
            url: "http://192.168.4.89",
            dataType: "html",
            cache: true,
            type: 'GET',
            
            converters: { "* text": window.String, "text html": true, "text json": self.parseJSON, "text xml": jQuery.parseXML },

            success: function (data) {

				topDestinations = $(data).find('div.hc_r_destinations div.hc_i');
				$('#notifications').empty().append('<h1>Top destinations</h1>').append(topDestinations);

				$('div.hc_i').on('click', function(){
					var newURL = $(this).find('a').attr('href');
					chrome.tabs.create({ url: newURL });
				});
            }
        });
	},

	updateSearchHistory: function(){

		var searchHistory = '';
		$.ajax({
            url: "http://192.168.4.89/SearchHistory",
            dataType: "html",
            cache: true,
            type: 'GET',
            
            converters: { "* text": window.String, "text html": true, "text json": self.parseJSON, "text xml": jQuery.parseXML },

            success: function (data) {

				noSearchHistoryHolder = $(data).find('#hc_evt_sh_none');
				searchHistory = $(data).find('#hc_evt_sh');
				searchHistory.find('a').each(function(index, item){
					var currentLink = $(item).attr('href');
					$(item).attr('href','http://192.168.4.89' + currentLink);
				});
				$('#notifications').empty().append('<h1>Your search history</h1>').append(noSearchHistoryHolder).append(searchHistory);

				$('div.hc_i').on('click', function(){
					var newURL = $(this).find('a').attr('href');
					chrome.tabs.create({ url: newURL });
				});
            }
        });
	}
}