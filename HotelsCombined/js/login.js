var HC = HC || {};

HC.Login = {

	loadSignInForm: function(){

		var topDestinations = '';
		$.ajax({
            url: "http://192.168.4.89:80/Account/Login",
            dataType: "html",
            cache: true,
            type: 'GET',
            
            converters: { "* text": window.String, "text html": true, "text json": self.parseJSON, "text xml": jQuery.parseXML },

            success: function (data, status, xhr) {

        		HC.Login.setLoginForm(data);
				
            }
        });
	},

	processAjaxSubmit: function(form){

		action = $(form).attr('action');
        method = $(form).attr('method');

        $.ajax({
            data: $(form).serialize(),
            url: action,
            type: method,
            cache: false,
            success: function (response) {
            	var loginForm = HC.Login.getLoginForm(response);
            	if(loginForm.length === 0){ // successful login
            		HC.Main.load('true');
            	} else {
        			HC.Login.setLoginForm(response);
            	}
            }
        });
	},

	getLoginForm : function(response){

		return $(response).find('div.hc_login');
	},

	setLoginForm : function(response){

		var errorHolder = $(response).find('span.hc_f_t_err5').first();
		var loginFormHolder = $(response).find('div.hc_login');
		loginFormHolder.find('a').removeAttr('onclick').removeAttr('href');
		loginFormHolder.find('script').remove();
		$('#content').empty().append('<link href="../../js/signin.css" rel="stylesheet" type="text/css">').append(errorHolder).append(loginFormHolder);

		loginFormHolder.find('form a').on('click', function (e) {
    		HC.Login.processAjaxSubmit($(this).closest('form'));
		});
	}
}