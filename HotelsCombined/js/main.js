var HC = HC || {};

HC.Main = {

	load: function(loggedIn){

		$.get( chrome.extension.getURL('src/browser_action/main.html'), function( data ) {
	    	$('#content').empty().append(data);
	    	HC.Main.init(loggedIn);
	    });
	},

	init: function(loggedIn){

        $("#checkin").datepicker({
            minDate: 0,
            dateFormat: "yy-mm-dd",
            maxDate: '+2y',
            onSelect: function(date){
                    var selectedDate = new Date(date);
                    var msecsInADay = 86400000;
                    var endDate = new Date(selectedDate.getTime() + msecsInADay);

                    $("#checkout").datepicker( "option", "minDate", endDate );
                    $("#checkout").datepicker( "option", "maxDate", '+2y' );

                }
            });

        $("#checkout").datepicker({
            minDate: 0,
            dateFormat: "yy-mm-dd"
        });

        $("#tabs").tabs();

        $('#searchButton').on('click', function(element) {

            var place = encodeURIComponent(autocomplete.getAutocompleteState().destination);
            var checkin = $("#checkin").val();
            var checkout = $("#checkout").val();
            var room = $( "#room" ).val();

            var newURL = "https://www.hotelscombined.com/Hotels/Search?destination="+place+"&radius=0km&checkin="+checkin+"&checkout="+checkout+"&"+room+"&pageSize=15&pageIndex=0&sort=Popularity-desc&showSoldOut=false&mapState=expanded";

            chrome.tabs.create({ url: newURL });
        });

        $('#tabs').on('tabsactivate', function(event, ui) {
    		var newIndex = ui.newTab.index();
    		if(newIndex === 1){
    			if(loggedIn === 'true'){
    				HC.Notifications.updateSearchHistory();
    			} else {
    				HC.Notifications.updateTopDestinations();
    			}
    		}
		});

        $('#signIn').on('click', function(element) {
        	HC.Login.loadSignInForm();
        });

		autocomplete = new HC.Autocomplete.AutocompleteInstance($('#place'), '', '');
	}
}