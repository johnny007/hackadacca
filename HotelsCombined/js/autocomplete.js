var HC = HC || {};

HC.Autocomplete = {

    _translationMoreOptions: null,
    _lastQuery: null,

    //Copy of jqueries with extra processing removed
    parseJSON: function (data) {

        if (typeof data !== "string" || !data) {
            return null;
        }

        // Attempt to parse using the native JSON parser first
        if (window.JSON && window.JSON.parse) {
            return window.JSON.parse(data);
        }

        return (new Function("return " + data))();

    },

    source: function (request, response, $input) {

        var self = HC.Autocomplete;
        var currentTerm = request.term;

        $.ajax({
            url: "http://www.hotelscombined.com/AutoUniversal.ashx",
            dataType: "json",
            cache: true,
            type: 'GET',
            data: {
                search: encodeURIComponent(request.term),
                limit: 10,
                languageCode: (window.navigator.userLanguage || window.navigator.language).substring(0,2),
                countryCode: 'AU'
            },


            converters: { "* text": window.String, "text html": true, "text json": self.parseJSON, "text xml": jQuery.parseXML },

            success: function (data) {

                if ($input.val() != currentTerm) {
                    return;
                }

                var mapped = $.map(data, function (item) {
                    var type = item.t.replace(/ /g, '');
                    var itemMapped = {
                        name: item.n,
                        place: item.p,
                        destination: item.k,
                        type: type,
                        typeName: item.tn,
                        countryCode: item.cc,
                        hotelCount: item.h,
                        resultID: item.ri,
                        searchID: item.si
                    };
                    itemMapped.label = self.formatName(itemMapped, false);
                    return itemMapped;
                });
                self._lastQuery = currentTerm;                
                response(mapped);
                
            }

        });

    },

    renderItem: function (ul, item, isNewType) {
        
        //Only render type name/type icon if isNewType is true.
        var self = HC.Autocomplete;
        var nameHtml = self.formatName(item, true);

        var extraClass = (isNewType) ? ' hc_t_newType' : '';

        return $('<li class="hc_t_' + item.type + extraClass + '"></li>')
			.data('ui-autocomplete-item', item)
            .append($('<a class="ui-corner-all" href="javascript:void(0);"></a>').append(nameHtml)
        ).appendTo(ul);

    },

    renderMenu: function (ul, items) {

        var self = this;        
        var currentType = null;
        var currentQuery = HC.Autocomplete._lastQuery;
        ul.css('z-index', 500);
        
        $.each(items, function (index, item) {
            var isNewType = false;
            if (item.type != currentType) {
                isNewType = true;
                currentType = item.type;
            }
            self._renderItem(ul, item, isNewType);
        });

        var showMore = HC.Autocomplete._translationMoreOptions
            .replace('[InsertTerm]', $('<div/>').text(currentQuery).html());
        
        $('<li class="hc_f_ac_searchmore"></li>')
            .append($('<a class="ui-corner-all" href="javascript:void(0);">' + showMore + '</a>'))
            .data('ui-autocomplete-item', { value: currentQuery, destination: 'none' })
            .appendTo(ul);

    },

    formatName: function (item, includeHtml) {

        var fullName = item.name;
        var place = '';
        
        if (item.place) {
            for (var i = 0; i < item.place.length; i++) {
                if (includeHtml && i == item.place.length - 1) {
                    place += ', <b>' + item.place[i] + '</b>';
                } else {
                    place += ', ' + item.place[i];
                } 
            }
        }

        if (includeHtml && !place)
            fullName = '<b>' + fullName + '</b>';

        fullName += place;

        if (includeHtml)
            fullName += this.renderHotelCount(item.hotelCount);

        return fullName;
        
    },

    renderHotelCount: function (count) {
        return ' <span class="hc_e_numHtls">(' + count + ')</span>';
    },

    AutocompleteInstance: function ($input, destinationName, destinationKey, options) {

        var _$input = $input;
        var _self = this;
        var _static = HC.Autocomplete;
        var _selectedDestination = destinationKey;
        var _selectedName = destinationName;
        var _initialDestination = destinationKey;
        var _initialName = destinationName;
        var _open = false;
        var _resultID;
        var _selectCallback;
        var _searchID;
        var rtl;
        
        options = options || {};

        _static._translationMoreOptions = 'Search more options for [InsertTerm]';
        
        //Work out the length
        var languageCode = (window.navigator.userLanguage || window.navigator.language).substring(0,2);
        var minChar = 2;
        if (languageCode === 'JA' || languageCode === 'CS' || languageCode === 'CN' || languageCode === 'KO') 
            minChar = 1;

        if (_$input.length > 0) {

            
            var position = { my: "left top", at: "left bottom", collision: "none" };
            
            _$input.autocomplete({

                source: function (request, response) {
                    _static.source(request, response, _$input);
                },

                minLength: minChar,
                appendTo: '#autocompleteResults',

                select: function (event, ui) {

                    var item = ui.item;

                    if (item.destination != 'none') {
                        _resultID = item.resultID;
                        _selectedDestination = item.destination;
                        _selectedName = item.value;
                        _searchID = item.searchID;
                    }

                    if (_selectCallback) {
                        _selectCallback(_$input);
                    }


                },
                autoFocus: false,
                delay: 5,

                open: function () {
                    _open = true;
                },

                close: function (event) {
                    _open = false;
                    event.preventDefault();
                },

                position: position

            });

            var $widget = _$input.autocomplete('instance');
            $widget._renderItem = _static.renderItem;
            $widget._renderMenu = _static.renderMenu;

            $widget.menu.element.addClass('hc_f_t_ac');
            $widget.menu.element.on('menuselect', function(event, ui) {
                ui.item.closest('li').addClass('ui-state-focus');
                ui.item.children('a').addClass('ui-state-hover');
            });

            /*Temporary hack until styles get moved over */
            var lastSelectedMenuAnchor;
            $widget.menu.element.on('menufocus', function (event, ui) {
                lastSelectedMenuAnchor = $(ui.item).removeClass('ui-state-focus').children('a').addClass('ui-state-hover');
            });

            $widget.menu.element.on('menublur', function () {
                if (lastSelectedMenuAnchor) {
                    lastSelectedMenuAnchor.removeClass('ui-state-hover');
                    lastSelectedMenuAnchor = null;
                }
            });

        }

        this.getAutocompleteState = function () {

            //Search boxes with no autocomplete
            if (_$input.length == 0) {
                return {
                    autocompleted: true,
                    nameChanged: false,
                    destination: _initialDestination,
                    typedName: _initialName
                };
            }

            var isAutocompleted = false;
            var nameChanged = false;
            var typedName = _$input.val();

            if (_selectedDestination && typedName && _selectedName && typedName.toLowerCase() == _selectedName.toLowerCase())
                isAutocompleted = true;

            if (_initialName && typedName && _initialName.toLowerCase() != typedName.toLowerCase())
                nameChanged = true;

            return {
                autocompleted: isAutocompleted,
                nameChanged: nameChanged,
                typedName: typedName,
                destination: isAutocompleted ? _selectedDestination : '',
                resultID: _resultID,
                searchID: _searchID
            };

        };

        this.getRedirectionUrl = function () {

            var state = this.getAutocompleteState();
            var path;

            if (state.autocompleted) {

                path = '/SearchTermTypeRedirection.ashx?destination='
                    + encodeURIComponent(state.destination);

                var lastQuery = _static._lastQuery;
                if (lastQuery && state.searchID) {
                    path += '&query='
                    + encodeURIComponent(lastQuery)
                    + '&searchID='
                    + encodeURIComponent(state.searchID)
                    + '&resultID='
                    + encodeURIComponent(state.resultID);
                }

            } else {
                path = '/Search.aspx?search=' + encodeURIComponent(state.typedName);
            }

            return path;

        };

        this.isOpen = function () {
            return _open;
        };

        this.setSelectCallback = function (callback) {
            _selectCallback = callback;
        };

        this.close = function() {
            _$input.autocomplete('close');
        };

    }

};
